<!-- back-to-top -->
<link href="resources/plugins/back-to-top/css/style.min.css" rel="stylesheet">
<script src="resources/plugins/back-to-top/js/modernizr.min.js"></script>
<script src="resources/plugins/back-to-top/js/main.min.js"></script>
<!-- End | back-to-top -->
<!-- owlcarousel2 -->
<link href="resources/plugins/owlcarousel2/css/owl.carousel.min.css" rel="stylesheet">
<link href="resources/plugins/owlcarousel2/css/owl.theme.default.min.css" rel="stylesheet">
<script src="resources/plugins/owlcarousel2/js/owl.carousel.min.js"></script>
<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay: 3000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
</script>
<!-- End | owlcarousel2 -->
<!-- parallax -->
<script type="text/javascript" src="resources/plugins/parallax/parallax.min.js"></script>
<link rel="stylesheet" type="text/css" href="resources/plugins/parallax/parallax.min.css">
<!-- End | parallax -->
<!-- ancora-suave -->
<script type="text/javascript" src="resources/plugins/ancora-suave/ancora-suave.min.js"></script>
<!-- End | ancora-suave -->
<!-- wow-scroll-animate -->
<script src="resources/plugins/wow-scroll-animate/js/index.min.js"></script>
<!-- End | wow-scroll-animate -->
</html>