<?php include'includes/header.php'; ?>
<title>Cliente</title>
<meta name="description" content="">
<meta name="Keywords" content="">
<meta name="author" content="Cliente">
<meta property="og:url" content="http://www.site.com.br/">
<meta property="og:type" content="website">
<meta property="og:image" content="resources/img/logo_share.png">
<meta property="og:title" content="">
<meta property="og:description" content="">
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Cliente",
  "url": "http://www.site.com.br/",
  "legalName": "Cliente",
  "email": "contato@site.com.br",
  "description": "",
  "alternateName": [
    "Cliente"
  ],
  "logo": "resources/img/logo_share.png",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "Taubaté", 
    "postalCode": "12020-070",
    "streetAddress": "Praça Monsenhor Silva Barros, 254 - Edifício Star Shop - 3º Andar - Sala 31"
  },
  "contactPoint": {
    "@type": "ContactPoint",
    "contactType": "Clinical Engineering",
    "email": "contato@site.com.br",
    "telephone": "+55-12-3632-6814",
    "url": "http://www.site.com.br/fale-conosco.php"
  }
}
</script>
</head>
<body>
<h1>Hello, world!</h1>

<div class="col-lg-6">
  Teste
</div>
<div class="col-lg-6">
  Testando
</div>

<section id="home" class="parallax wow fadeInBottom" data-speed="15"></section>
<section id="portfolio" class="parallax wow fadeInBottom" data-speed="10"></section>

<div style="margin-top: 1000px">
<img class="wow fadeInLeft" src="resources/img/nebuleuse-helice.jpg" width="320">
</div>

<div class="owl-carousel owl-theme">
  <div class="item"><h4>1</h4></div>
  <div class="item"><h4>2</h4></div>
  <div class="item"><h4>3</h4></div>
  <div class="item"><h4>4</h4></div>
  <div class="item"><h4>5</h4></div>
  <div class="item"><h4>6</h4></div>
  <div class="item"><h4>7</h4></div>
  <div class="item"><h4>8</h4></div>
  <div class="item"><h4>9</h4></div>
  <div class="item"><h4>10</h4></div>
  <div class="item"><h4>11</h4></div>
  <div class="item"><h4>12</h4></div>
</div>

<a href="#0" class="cd-top">Top</a>
</body>
<?php include'includes/footer.php' ?>